package ru.mirea.vidyakin_i_n.toastapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText edtxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtxt = findViewById(R.id.editTextTextPersonName);
    }

    public void onClickToast(View view) {
        Toast.makeText(this, "СТУДЕНТ №7 ГРУППА БСБО-01-20 Количество символов - "+ edtxt.getText().toString().length(), Toast.LENGTH_SHORT).show();
    }
}